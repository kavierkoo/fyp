<?php

namespace App\Http\Controllers;

use App\Classe;
use App\Intake;
//use Illuminate\Http\Request;
use App\Module;
use App\Qr;
use App\User;
use auth;
use Illuminate\Support\Facades\DB;
use Request;
use Illuminate\Support\Facades\Input;

class LecturerController extends Controller
{
    public function __construct()
    {
        $this->middleware('lecturer');
    }
    public function index()
    {
        return view('/');
    }
    public function generate_qr()
    {
        $module = Module::all();
        $module_code = Module::where('user_email',Auth::user()->email)->pluck('module_code', 'module_code');
        $intake = Module::select('intake_code as ""')->where('user_email',Auth::user()->email)->get();

        $intake =  str_replace(str_split('{}[]\\/:*?"<>|'),'',(explode(',',$intake)));


        return view('generate_qr',compact('module','module_code','intake'));
    }

    public function showqr()
    {
        $qr_id = input::get('qrid');
        return view('showqr');
    }

    public function expired(){
        // Expired Specific QR

        // Grab QR ID
        $qr_id = input::get('qrid');

        DB::table('qrs')
            ->where('id', $qr_id)
            ->update(['status' => 'expired']);
        \Session::flash('flash_success', 'QR code has been set Expired!');
        return view('/home');
    }

    public function showqr_result(){


        return view('/showqr_result');
    }


    public function receive(\Illuminate\Http\Request $request)
    {;
        $this ->validate($request,[
            'Class_Date' => 'required|date|before:tomorrow',
            'module' => 'required',
            'intake' => 'required',
            'class_type' => 'required'
        ]);

        $date=Request::input('Class_Date');
        $module=Request::input('module');
        $intake=implode(',',Input::get('intake'));
        $type=Request::input('class_type');

        // Cross check module and intake
        $check = Module::select('intake_code as ""')->where('user_email','=',Auth::user()->email)->where('module_code','=',$module )->get();

        // check module and intake
        if(strpos($check,$intake))
        {
            //query to match
            $match = [
                'class_date' => $date,
                'lecturer_email' => Auth::user()->email,
                'module_code' => $module,
                'intake_code' => $intake,
                'class_type'=>$type,
            ];

            // check duplicated qr
            $check_duplicate = Qr::select('id as ""')->where($match)->get();



            //check duplicate qr
            if($check_duplicate->isEmpty()){

                //check duplicated qr for specify intake
                $check_intake = str_replace(str_split('{}[]\\/:*?"<>|'),'',(Qr::select('intake_code as ""')->where([
                    'class_date' => $date,
                    'lecturer_email' => Auth::user()->email,
                    'module_code' => $module,
                    'class_type'=>$type,
                ])->get()));

                //Check duplicate qr intake
                if ($check_intake != null){
                    if(strpos($intake,$check_intake)!== false){
                        return back()->withInput()->withErrors('Duplication Detected! The QR Code has been generated!');
                    }
                }

                // Pass Duplication validation
                // save to db
                Qr::create([
                    'class_date' => $date,
                    'lecturer_email' => Auth::user()->email,
                    'module_code' => $module,
                    'intake_code' => $intake,
                    'class_type' => $type,
                    'status' => 'active'
                ]);

                /* Abandon - Inefficient Code
                // Grab IP from db
                $ip = User::select('intake_id as ""')->where('email','=','kavier@test.com')->get();
                $ip =  str_replace(str_split('{}[]\\*"<>|'),'',$ip);
                $ip= ltrim($ip, ':');

                */

                //get qr id
                $qrcontent = Qr::select('id as ""')->where($match)->get();
                $qrid =  str_replace(str_split('{}[]\\/:*?"<>|'),'',$qrcontent);
                $url = Request::url();
                $url = str_replace('showqr',"confirm?id=",$url);
                $qrcontent = $url. $qrid;


                //return $qrcontent;
                return view('showqr',compact('qrcontent','qrid'));
            }
            else{
                return back()->withInput()->withErrors('Duplication Detected! The QR Code has been generated!');
            }

        }
        else{
            return back()->withInput()->withErrors('Whoops! The Module was not falls under Intakes selected! Please try again!');
        }

    }

    public function lecturer_reports()
    {
        // Class Attendance
        $modules = Module::where('user_email','=',Auth::user()->email)->get(); // Intake obtained as well
        $attendance_records = Classe::all()->sortByDesc('qr_id');

        // Generated QR Records
        $qr_records = Qr::where('lecturer_email','=',Auth::user()->email)->get()->sortByDesc('id');

        // Student Attendance
        return view ('/lecturer_reports',compact ('attendance_records','qr_records','modules'));
    }


}

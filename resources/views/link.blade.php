@extends('layouts.app')

<!-- This view will be handling both ADD INTAKE and ADD MODULE-->
@section('content')
    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
            <div class="panel-heading">Link Relationship</div>
            <div class="panel-body ">
                <legend>
                    Link {{$add_menu}} to {{$add_id}}

                        <button type="button" onclick="showbtn()" class="btn btn-default btn-xs " id="link_show" aria-label="Center Align">
                            <span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span>
                        </button>
                        <button type="button" onclick="hidebtn()" class="btn btn-default btn-xs hidden" id="link_hide" aria-label="Center Align">
                            <span class="glyphicon glyphicon-chevron-up" aria-hidden="true"></span>
                        </button>

                </legend>


            <!--Start of Form-->
            {!! Form::open(['class'=>'form-horizontal','url'=>$url]) !!}

            <!--Class Type-->

                @if($add_menu == 'module')
                    <!--show record-->
                        <div id="link_hide_show" class="hidden">
                            <div class="well " style=" background: #FFFFFF; padding: 5px; margin-bottom: 5px;">
                                @if(count($all_modules) == 0)
                                    <h4 class="text-center"style="padding: 10px">No Data!</h4>
                                @else
                                    @if($add_id == 'intake')
                                        <table class="table table-striped  text-center "style="margin-bottom: 0px;">
                                            <thead>
                                            <tr>
                                                <th class="text-center">Module Code</th>
                                                <th class="text-center">Intake Code</th>
                                            </tr>
                                            </thead>
                                            <tbody>

                                            @foreach($all_modules as $all_module)
                                                <tr>
                                                    <td>{{$all_module->module_code}}</td>
                                                    <td>{{$all_module->intake_code}}</td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    @elseif($add_id == 'lecturer')
                                        <table class="table table-striped  text-center "style="margin-bottom: 0px;">
                                            <thead>
                                            <tr>
                                                <th class="text-center">Module Code</th>
                                                <th class="text-center">Lecturer Email</th>
                                            </tr>
                                            </thead>
                                            <tbody>

                                            @foreach($all_modules as $all_module)
                                                <tr>
                                                    <td>{{$all_module->module_code}}</td>
                                                    <td>{{$all_module->user_email}}</td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    @endif
                                @endif
                            </div> <!-- End of Well -->
                        </div>

                        <br>
                        <!--Module-->
                    <div class="form-group">
                        {!! Form::label('Module',null,['class'=>'col-sm-2 control-label']) !!}
                        <div class ="col-sm-10">
                            {!!Form::select('module_code',$modules, null,['placeholder' => 'Choose Module','class'=>'form-control'])!!}
                        </div>
                    </div>
                    <!--Link module To intake-->
                    @if($add_id == 'intake')
                        <!--intake-->
                            <!--Intake-->
                            <div class="form-group">
                                {!! Form::label('Intake :',null,['class'=>'col-sm-2 control-label']) !!}
                                <div class="col-sm-10">

                                    @foreach($intakes as $intake)
                                        {!! Form::checkbox('intake_code[]', $intake)!!} {{$intake}}
                                        <br>
                                    @endforeach

                                </div>
                            </div>
                    <!--Link module To lecturer-->
                    @elseif($add_id == 'lecturer')
                        <!--lecturers-->
                            <div class="form-group">
                                {!! Form::label($add_id,null,['class'=>'col-sm-2 control-label']) !!}
                                <div class ="col-sm-10">
                                    {!!Form::select('email',$lecturers, null,['placeholder' => 'Choose Lecturer','class'=>'form-control'])!!}
                                </div>
                            </div>
                    @endif
                <!--Link intake To student-->
                @elseif($add_menu =='intake')
                    <div class="well hidden" id="link_hide_show" style=" background: #FFFFFF; padding: 5px; margin-bottom: 5px;">
                        <table class="table table-striped  text-center "style="margin-bottom: 0px;">
                            <thead>
                                <tr>
                                    <th class="text-center">Student Email</th>
                                    <th class="text-center">Student Intake</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($all_students as $all_student)
                                    <tr>
                                        <td>{{$all_student->email}}</td>
                                        <td>{{$all_student->intake_id}}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                        <br>
                        <!--student-->
                        <div class="form-group">
                            {!! Form::label($add_id,null,['class'=>'col-sm-2 control-label']) !!}
                            <div class ="col-sm-10">
                                {!!Form::select('email',$students, null,['placeholder' => 'Choose Student','class'=>'form-control'])!!}
                            </div>
                        </div>
                        <!--Intake-->
                        <div class="form-group">
                            {!! Form::label('Intake',null,['class'=>'col-sm-2 control-label']) !!}
                            <div class ="col-sm-10">
                                {!!Form::select('intake_code',$intakes, null,['placeholder' => 'Choose Intake','class'=>'form-control'])!!}
                            </div>
                        </div>
                @endif
                <!--Button-->
                <div class="text-center">
                    {!!  Form::submit('Create',['class'=>'btn btn-primary ']) !!}
                </div>

            {!! Form::close() !!} <!--End of Form-->


            </div>
        </div>
    </div>
@endsection
@section('footer')
    <script>
        function hidebtn() {
            $("#link_hide_show").addClass('hidden');
            $("#link_hide").addClass('hidden');
            $("#link_show").removeClass('hidden');
        }
        function showbtn() {
            $("#link_hide_show").removeClass('hidden');
            $("#link_hide").removeClass('hidden');
            $("#link_show").addClass('hidden');
        }
    </script>
@endsection
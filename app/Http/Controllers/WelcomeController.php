<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\View_count;

class WelcomeController extends Controller
{
    public function welcome()
    {
        // Record View Count
        $count = View_count::where('view_name','welcome')->first();
        $count->view_count =$count->view_count+1 ;
        $count -> save();

        return view('/welcome')->with('count',$count);
    }
}

@extends('layouts.app')

@section('content')


    <div class="panel panel-default">
        <div class="panel-heading">Attendance Records</div>
        <div class="panel-body"style="padding-top: 10px">

            <!-- Tab bar-->
            <ul class="nav nav-tabs">
                <li class="active"><a href="#summary">Summary</a></li>
                <li><a href="#qr_history">QR History</a></li>
            </ul>

            @if($status == 0)
            <h4 class="text-center"style="padding: 10px">No Intake Allocated to this account, Please contact admin! </h4>
            @else
            <!-- Tab's content-->
            <div class="tab-content">
                <div id="summary" class="tab-pane fade in active">
                    <div class="well " style=" background: #FFFFFF; padding: 5px; margin-bottom: 5px;">
                        <legend style="padding-left: 5px;margin-top: 5px;margin-bottom: 5px">Attendance Summary</legend>
                        @if(count($attendance_records) == 0)
                            <h4 class="text-center"style="padding: 10px">No Attendance Summary!</h4>
                        @else
                            <table class="table table-striped  text-center "style="margin-bottom: 0px;">
                                <thead>
                                <tr>
                                    <th class="text-center">Module Code</th>
                                    <th class="text-center">Percentage</th>
                                    <th class="text-center">Total Classes</th>
                                </tr>
                                </thead>
                                <tbody>

                                @foreach($attendance_records as $attendance_record)
                                    <tr>
                                        <td>{{$attendance_record['module_code']}}</td>
                                        <td>
                                            @if($attendance_record['total'] == 0 )
                                                100
                                            @else
                                                {{$attendance_record['percent']}}
                                            @endif
                                            %
                                        </td>
                                        <td>{{$attendance_record['present']}}/{{$attendance_record['total']}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                        @endif
                    </div> <!-- End of Well -->

                </div>
                <!-- Scanned QR Records-->
                <div id="qr_history" class="tab-pane fade">
                    <div class="well " style=" background: #FFFFFF; padding: 5px; margin-bottom: 5px;">
                        <legend style="padding-left: 5px;margin-top: 5px;margin-bottom: 5px">Scanned QR Records</legend>
                        @if(count($qr_records) == 0)
                            <h4 class="text-center"style="padding: 10px">No Scanned History!</h4>
                        @else
                            <table class="table table-striped  text-center "style="margin-bottom: 0px;">
                                <thead>
                                <tr>
                                    <th class="text-center">Class Date</th>
                                    <th class="text-center">Module Code</th>
                                    <th class="text-center">Class Type</th>
                                    <th class="text-center">Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($qr_records as $qr_record)
                                    <tr>
                                        <td>{{$qr_record->class_date}}</td>
                                        <td>{{$qr_record->module_code}}</td>
                                        <td>{{$qr_record->intake_code}}</td>
                                        <td>{{$qr_record->class_type}}</td>
                                        <td>{{$qr_record->attendance}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @endif
                    </div> <!-- End of Well -->
                </div> <!-- end of QR_History-->
            </div> <!-- end of Tab-content -->
            @endif
        </div> <!-- end of <div class="panel-body">-->
    </div> <!-- end of <div class="panel panel-default">-->


@endsection

@section('footer')
    <script>
        $(document).ready(function(){
            $(".nav-tabs a").click(function(){
                $(this).tab('show');
            });
        });
    </script>

@endsection
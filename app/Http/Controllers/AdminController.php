<?php

namespace App\Http\Controllers;

use App\Classe;
use App\Intake;
use App\Qr;
use App\User;
use Request;
use Session;
use Artisan;
use App\Module;
use App\View_count;



class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }
    public function index()
    {
        return view('/');
    }

    public function maintenance()
    {
        // Show maintenance view
        return view('/maintenance');
    }

    public function change_maintenance()
    {
        $action=Request::input('status');
        //Disable due to limited by hosting site

        if($action=='Activate')
        {
            Artisan::call('up');
            return redirect('/maintenance')->with('flash_success', 'The site is now back to Live!');
        }
        elseif($action=='Deactivate')
        {
            Artisan::call('down');
            return redirect('/maintenance')->with('flash_warning', 'The site is now in Maintenance Mode!');
        }
        elseif($action=="0")
        {
            return redirect('/maintenance')->with('flash_error', 'Oops! Please choose an action before proceeding!');
        }
        else
        {
            //Disabled due to hosting
            //return redirect('/maintenance')->with('flash_error', 'Oops! Something Wrong! Please resubmit the request');
            return redirect('/maintenance')->with('flash_error', 'Due to hosting reason, This function has been disabled!');
        }

    }

    public function add_intake()
    {
        // Return add_new view with ADD INTAKE Function
        $add_id = 'intake';
        $url = '/add_intake';
        return view('/add_new',compact('add_id','url'));
    }

    public function add_module()
    {
        // Return add_new view with ADD MODLE Function
        $add_id = 'module';
        $url = '/add_module';
        return view('/add_new',compact('add_id','url'));
    }

    public function save_intake(\Illuminate\Http\Request $request)
    {
        // Save intake

        // Check Validation
        $this ->validate($request,[
            'intake_code' => 'required|unique:intakes',
        ]);
        $code=Request::input('intake_code');

        try
        {
            // Attempt to save to DB
            Intake::create([
                'intake_code' => $code,
            ]);
            return redirect('/add_intake')->with('flash_success','New Intake Added to the system!');
        }
        catch (\Exception $e)
        {
            // Return error if wrong
            return redirect()->back()->withErrors('Oops! Something went wrong, Please submit again!');
        }
    }

    public function save_module(\Illuminate\Http\Request $request)
    {
        // Check validation
        $this ->validate($request,[
            'module_code' => 'required|unique:modules',
            'module_name' => 'required|unique:modules',
        ]);
        $code=Request::input('module_code');
        $name=Request::input('module_name');

        try
        {
            // Attempt to save to DB
            Module::create([
                'module_code' => $code,
                'module_name' => $name,
                'intake_code' => '',
                'user_email' => '',
            ]);
            return redirect('/add_module')->with('flash_success','New Module Added to the system!');
        }
        catch (\Exception $e)
        {
            // Return error if wrong
            return redirect()->back()->withErrors('Oops! Something went wrong, Please submit again!');
        }
    }
    public function link_intake()
    {
        // Set default value
        $add_menu = 'module';
        $add_id = 'intake';
        $url = '/link_module_intake';

        // Fetch value from db
        $modules = Module::all()->pluck('module_code', 'module_code');
        $intakes = Intake::all()->pluck('intake_code', 'intake_code');
        $all_modules = Module::all();

        return view('/link',compact('add_menu','add_id','all_modules','url','modules','intakes'));
    }
    public function link_lecturer()
    {
        // Set default value
        $add_menu = 'module';
        $add_id = 'lecturer';
        $url = '/link_module_lecturer';
        $all_modules = Module::all();

        // Set fetch value from db
        $modules = Module::all()->pluck('module_code', 'module_code');
        $lecturers = User::all()->where('user_type','2')->pluck('email', 'email');
        return view('/link',compact('add_menu','add_id','all_modules','url','modules','lecturers'));
    }
    public function link_student()
    {
        // Set default value
        $add_menu = 'intake';
        $add_id = 'student';
        $url = '/link_intake_student';
        $all_students = User::where('user_type',3)->get();

        // Set fetch value from db
        $intakes = Intake::all()->pluck('intake_code', 'intake_code');
        $students = User::all()->where('user_type','3')->pluck('email', 'email');
        return view('/link',compact('add_menu','add_id','all_students','url','intakes','students'));
    }

    public function save_link_intake(\Illuminate\Http\Request $request)
    {
        // Check validation
        $this ->validate($request,[
            'module_code' => 'required',
            'intake_code' => 'required',
        ]);
        $module=Request::input('module_code');
        $intake=implode(',',Request::input('intake_code'));

        try
        {
            // Attempt to update to DB
            Module::where('module_code',$module)->update(['intake_code'=>$intake]);


            return redirect('/link_module_intake')->with('flash_success',"Modules's Intake Code Successfully modified!");
        }
        catch (\Exception $e)
        {
            // Return error if wrong
            return redirect()->back()->withErrors('Oops! Something went wrong, Please submit again!');
        }
    }
    public function save_link_lecturer(\Illuminate\Http\Request $request)
    {
        // Check validation
        $this ->validate($request,[
            'module_code' => 'required',
            'email' => 'required', // will then change to user_email
        ]);
        $module=Request::input('module_code');
        $email=Request::input('email'); // will then change to user_email

        try
        {
            // Attempt to update to DB
            Module::where('module_code',$module)->update(['user_email'=>$email]);
            return redirect('/link_module_lecturer')->with('flash_success',"Modules's Lecturer Email Successfully modified!");
        }
        catch (\Exception $e)
        {
            // Return error if wrong
            return redirect()->back()->withErrors('Oops! Something went wrong, Please submit again!');
        }
    }
    public function save_link_student(\Illuminate\Http\Request $request)
    {
        // Check validation
        $this ->validate($request,[
            'email' => 'required',
            'intake_code' => 'required', // will then change to user_email
        ]);
        $email=Request::input('email');
        $intake_code=Request::input('intake_code'); // will then change to user_email

        try
        {
            // Attempt to update to DB
            User::where('email',$email)->update(['intake_id'=>$intake_code]);
            return redirect('/link_intake_student')->with('flash_success',"Student's Intake Successfully modified!");
        }
        catch (\Exception $e)
        {
            // Return error if wrong
            return redirect()->back()->withErrors('Oops! Something went wrong, Please submit again!');
        }
    }

    // Scanned QR Code
    public function generatedQR()
    {
        $qr_records = Qr::all()->sortByDesc('id');  // Retrieve scanned record from QR DB sort by Date
        $attendance_records = Classe::all()->sortByDesc('qr_id');
        return view ('/admin_reports',compact ('attendance_records','qr_records'));
    }

    public function info()
    {
        $intakes = Intake::all()->sortByDesc('id');
        $modules = Module::all()->sortByDesc('id');
        $users = User::all()->sortByDesc('id');
        return view ('/info',compact('intakes','modules','users'));
    }

}

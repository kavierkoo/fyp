<style>
    .custom
    {
        text-align: center;
        color: #D8000C;
        font-weight: 100;
        font-family: 'Lato', sans-serif;
        font-size: 72px;
    }
</style>
@extends('layouts.app')

@section('title')
    Error 404
@endsection

<!-- Inside Container -->
@section('content')

    <div class="custom" style=" background-color: #FFBABA;"> <span class="glyphicon glyphicon glyphicon-alert" aria-hidden="true">  </span> ERROR 403! <br></div>
    <br>
    <div class="custom" style="font-size: 50px;color: #2e3436;">Oops! Access denied! :(</div>
    <div class="custom" style="font-size: 30px;color: #2e3436;"> You don't have permission to open this page. Please Check Middleware!</div>
    <br>

    @if(Auth::guest())
        <div class="custom" style="font-size: 20px;color: #2e3436;">
            Sign in is required! If you wish to login now, Click the button below!
            <br><br>
            <a href="{{ url('/login') }}" class="btn btn-primary  btn-lg" role="button">Login</a>
        </div>
    @else
        <div class="custom" style="font-size: 20px;color: #2e3436;">
            You're still singed in. If you wish to sign out, Click the button below!
            <br><br>
            {{Auth::logout()}}
            <a href="{{ url('/login') }}" class="btn btn-primary  btn-lg" role="button">Logout</a>
            <!--
            <button type="button"class="btn btn-primary  btn-lg" href="{{ url('/logout') }}"onclick="event.preventDefault();
            document.getElementById('logout-form').submit();">Logout</button>
            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
            -->
        </div>
    @endif



@endsection


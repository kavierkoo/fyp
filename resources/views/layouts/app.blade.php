
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'QR-ATS') }}</title>

    <!-- Styles -->
	<!-- ****** favicons ****** -->
	<link rel="shortcut icon" href="public/img/favicon/favicon.ico">
	<link rel="icon" sizes="16x16 32x32 64x64" href="public/img/favicon/favicon.ico">
	<link rel="icon" type="image/png" sizes="196x196" href="public/img/favicon/favicon-192.png">
	<link rel="icon" type="image/png" sizes="160x160" href="public/img/favicon/favicon-160.png">
	<link rel="icon" type="image/png" sizes="96x96" href="public/img/favicon/favicon-96.png">
	<link rel="icon" type="image/png" sizes="64x64" href="public/img/favicon/favicon-64.png">
	<link rel="icon" type="image/png" sizes="32x32" href="public/img/favicon/favicon-32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="public/img/favicon/favicon-16.png">
	<link rel="apple-touch-icon" href="public/img/favicon/favicon-57.png">
	<link rel="apple-touch-icon" sizes="114x114" href="public/img/favicon/favicon-114.png">
	<link rel="apple-touch-icon" sizes="72x72" href="public/img/favicon/favicon-72.png">
	<link rel="apple-touch-icon" sizes="144x144" href="public/img/favicon/favicon-144.png">
	<link rel="apple-touch-icon" sizes="60x60" href="public/img/favicon/favicon-60.png">
	<link rel="apple-touch-icon" sizes="120x120" href="public/img/favicon/favicon-120.png">
	<link rel="apple-touch-icon" sizes="76x76" href="public/img/favicon/favicon-76.png">
	<link rel="apple-touch-icon" sizes="152x152" href="public/img/favicon/favicon-152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="public/img/favicon/favicon-180.png">
	<meta name="msapplication-TileColor" content="#FFFFFF">
	<meta name="msapplication-TileImage" content="public/img/favicon/favicon-144.png">
	<meta name="msapplication-config" content="public/img/favicon/browserconfig.xml">
	<!-- ****** favicons ****** -->
	
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!--As Backup-->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">-->

    <style>
        body
        {
            background: #EAEAEA !important;
        }

    </style>

    <!-- Scripts -->
    <script>

        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>

    </script>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top" style="margin-bottom:0px;">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    @if (Auth::guest())
                        <!-- Branding Image -->
                        <a class="navbar-brand" href="{{ url('/') }}"> <span class="glyphicon glyphicon glyphicon-qrcode" aria-hidden="true">  </span>
                            {{ config('app.name', 'QR-ATS') }}
                        </a>
                    @elseif (Auth::check())
                        <a class="navbar-brand" href="{{ url('/home') }}"> <span class="glyphicon glyphicon glyphicon-qrcode" aria-hidden="true">  </span>
                            {{ config('app.name', 'QR-ATS') }}
                        </a>
                    @else
                        abort(404);
                    @endif
                </div>

                <!--Testing Purposes-->


                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Authentication Links -->
                    <!-- Guess Access-->
                    @if (Auth::guest())
                        <!-- Right Side Of Navbar -->
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="{{ url('/login') }}">Login</a></li>
                        </ul> <!-- End of Nav Bar-->

                    <!--Logged in Access-->
                    @else
                        <!-- Admin Access-->
                        @if(Auth::user()->user_type == 1 )

                            <!-- Left Side Of Navbar -->
                            <ul class="nav navbar-nav">
                                <!-- Content Start Here -->
                                <!-- Start of Create drop down-->
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                        Create
                                        <span class="caret"></span>
                                    </a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="{{ url('/register') }}">Create User</a></li>
                                        <li><a href="{{ url('/add_intake') }}">Create Intake</a></li>
                                        <li><a href="{{ url('/add_module') }}">Create Module</a></li>
                                    </ul> <!--End of dropdown menu-->
                                </li>
                                <!-- End of Create drop down-->

                                <!-- Start of Link drop down-->
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                        Link
                                        <span class="caret"></span>
                                    </a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="{{ url('/link_module_intake') }}">Link Module to Intake</a></li>
                                        <li><a href="{{ url('/link_module_lecturer') }}">Link Module to Lecturer</a></li>
                                        <li><a href="{{ url('/link_intake_student') }}">Link Intake to Student</a></li>
                                    </ul> <!--End of dropdown menu-->
                                </li><!-- End of dropdown -->
                                <!-- End of Link drop down-->

                                <!-- Start of Report drop down-->
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                        Report
                                        <span class="caret"></span>
                                    </a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="{{ url('/admin_reports') }}">Scanned QR Code</a></li>
                                        <li><a href="{{ url('/info') }}">QR-ATS Information</a></li>
                                    </ul> <!--End of dropdown menu-->
                                </li><!-- End of dropdown -->
                                <!-- End of Report drop down-->


                                <li><a href="{{ url('/maintenance') }}">Maintenance Page</a></li>

                            </ul><!--End of Left Nav Bar -->

                                <!-- Right Side Of Navbar -->
                            <ul class="nav navbar-nav navbar-right">

                                <!-- Right Dropdown bar -->
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                        {{ Auth::user()->name }} <span class="caret"></span>

                                    </a>
                                    <ul class="dropdown-menu" role="menu">

                                        <!-- Content Start Here -->
                                        <li class="dropdown-header"><strong>Admin Role</strong></li>
                                        <li role="separator" class="divider"></li>

                                        <li><a href="{{ url('/logout') }}"onclick="event.preventDefault();
                                                document.getElementById('logout-form').submit();">Logout</a>
                                            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                                {{ csrf_field() }}
                                            </form>
                                        </li>

                                    </ul> <!--End of dropdown menu-->
                                </li><!-- End of dropdown -->
                            </ul><!--End of Nav Right -->
                         
                        <!--Lecturer Access-->
                        @elseif(Auth::user()->user_type== 2 )

                            <!-- Left Side Of Navbar -->
                                <ul class="nav navbar-nav">

                                    <!-- Content Start Here -->
                                    <li><a href="{{ url('/generate_qr') }}">Generate QR Code</a></li>
                                    <li><a href="{{ url('/lecturer_reports') }}">Reports</a></li>
                                </ul><!--End of Left Nav Bar -->

                            <!-- Right Side Of Navbar -->
                            <ul class="nav navbar-nav navbar-right">

                                <!-- Right Dropdown bar -->
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                        {{ Auth::user()->name }} <span class="caret"></span>
                                    </a>
                                    <ul class="dropdown-menu" role="menu">

                                        <!-- Content Start Here -->
                                        <li class="dropdown-header"><strong>Lecturer Role</strong></li>
                                        <li role="separator" class="divider"></li>
                                        <li><a href="{{ url('/logout') }}"onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();">Logout</a>
                                            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                                {{ csrf_field() }}
                                            </form>
                                        </li>
                                    </ul> <!--End of dropdown menu-->
                                </li><!-- End of dropdown -->
                            </ul><!--End of Nav Right -->

                        <!--Student Access-->
                        @else

                            <!-- Left Side Of Navbar -->
                                <ul class="nav navbar-nav">

                                    <!-- Content Start Here -->
                                    <li><a href="student_attendance">Attendance Records</a></li>
                                </ul><!--End of Left Nav Bar -->

                            <!-- Right Side Of Navbar -->
                            <ul class="nav navbar-nav navbar-right">

                                <!-- Right Dropdown bar -->
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                        {{ Auth::user()->name }} <span class="caret"></span>
                                    </a>
                                    <ul class="dropdown-menu" role="menu">

                                        <!-- Content Start Here -->
                                        <li class="dropdown-header"><strong>Student Role</strong></li>
                                        <li role="separator" class="divider"></li>
                                        <li><a href="{{ url('/logout') }}"onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();">Logout</a>
                                            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                                {{ csrf_field() }}
                                            </form>
                                        </li>
                                    </ul> <!--End of dropdown menu-->
                                </li><!-- End of dropdown -->
                            </ul><!--End of Nav Right -->
                        @endif
                    @endif
                </div><!-- End of Nav Bar-->
            </div><!-- End of Container -->
        </nav><!-- End of Header-->

        @if(auth::guest() == false)
            <!--Fetch user information -->
            <div id="info_bar" class="well hidden" style=" background: #f9f9f9; padding: 0px; border-top:0px;margin-bottom: 0px">
                <div class="container">
                    <table class="table table-striped  text-center" style="margin-bottom: 0px;">
                        <tbody>
                            <tr>
                                <td><b>Name : </b></td>
                                <td>{{Auth::user()->name}}</td>
                                <td><b>Email : </b></td>
                                <td>{{Auth::user()->email}}</td>


                                @if(Auth::user()->intake_id == null) <!-- No Intake -->
                                    @if(Auth::user()->user_type == 1)
                                        <td><b>Role : </b></td>
                                        <td>Admin</td>
                                    @elseif(Auth::user()->user_type == 2)
                                        <td><b>Role : </b></td>
                                        <td>Lecturer</td>
                                    @endif

                                @else<!-- got Intake -->
                                    <td><b>Intake : </b></td>
                                    <td>
                                        {{Auth::user()->intake_id}}
                                    </td>
                                    @endif

                                <td><b>Register Date : </b></td>
                                <td>{{Auth::user()->created_at}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <!--Hide and Show info Bar-->
            <div class="text-center" id="info_bar_btn">
                <button type="button" onclick="showbtn()" class="btn btn-default btn-xs" id="show" aria-label="Center Align">
                    <span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span>
                </button>
                <button type="button" onclick="hidebtn()" class="btn btn-default btn-xs hidden" id="hide" aria-label="Center Align">
                    <span class="glyphicon glyphicon-chevron-up" aria-hidden="true"></span>
                </button>
            </div>
        @endif


        <br>
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <!-- Pop up  alert -->
                    <!--
                    To call this function
                     //Popup
                    if($saved) //Success
                    {
                        \Session::flash('flash_success', 'Successfully Added New User!');

                    }
                    else //fail
                        \Session::flash('flash_error', 'Whoops! Something went wrong! Please try again!');
                    }
                    -->


                    @if(Session::has('flash_success'))
                        <div class="alert alert-success alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                            <strong>{{ Session::get('flash_success')}}</strong>
                        </div>
                    @elseif(Session::has('flash_error'))
                        <div class="alert alert-danger alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                            <strong>{{ Session::get('flash_error')}}</strong>
                        </div>
                    @elseif(Session::has('flash_warning'))
                        <div class="alert alert-warning alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                            <strong>{{ Session::get('flash_warning')}}</strong>
                        </div>
                    @endif

                </div>
            </div>

            <!--Validator-->
            @if(isset($errors) && $errors->any())
                <div class="col-md-8 col-md-offset-2 alert alert-danger alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <strong>
                        @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </strong>
                </div>
            @endif

            <!--Content-->
            @yield('content')
        </div>
    </div><!-- End of Container -->



    <!-- Scripts -->
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0-alpha1/jquery.min.js"></script>
    <!--As backup-->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script>
        function hidebtn() {
            $("#info_bar").addClass('hidden');
            $("#hide").addClass('hidden');
            $("#show").removeClass('hidden');
        }
            function showbtn() {
            $("#info_bar").removeClass('hidden');
            $("#hide").removeClass('hidden');
            $("#show").addClass('hidden');
            }

        /*
        $(function(){
            $('#btmhide').on('click',function() {
                $("#info_bar").addClass('hidden');
            });
        });

        $(function(){
            $('#btmshow').on('click',function() {
                $("#info_bar").removeClass('hidden');
            });
        });
        */
    </script>
    @yield('footer')
    <footer style="text-align: center;">
        <br><br>
        <!-- Feedback form Link-->
        <h4>
            QR-ATS's developer would kindly request you to spare a few minutes of your valuable time to fill in the feedback form.
            <br>
            All data will purely serves for academic research purposes and all data will be kept confidential!
            <br>
            <a href="https://goo.gl/forms/eGE43F7KGSvxdW9k2" target="_blank">QR-ATS Feedback Form</a>
        </h4>


        @if(isset($count))
            <h4>
                QR-ATS has been visited {{$count->view_count}} time by visitors!
                <br>
                Latest visit at {{$count->updated_at}}
            </h4>
        @endif


        <p>
            QR-ATS © 2016 Asia Pacific University All Right Reserved
            <br>
            To view the User Manual, Please click <a href="https://drive.google.com/file/d/0BySNOzogzCU5bzJQQVkwcENGWTg/view"  target="_blank">here</a>
            <br>
            For more information, please contact developer via <a href="http://www.kavierkoo.com">kavierkoo.com</a>.
        </p>
    </footer>
</body>
</html>

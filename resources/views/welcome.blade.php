@extends('layouts.app')
@section('title')
    FYP Kavier Tittle
@endsection



@section('content')

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Welcome to QR-ATS</div>
                <div class="panel-body">
                    <img class="center-block" src={{"img/qr.png"}} class="img-rounded">
                    <h1 class="text-center">QR-code Attendance Taking System </h1>
                    <h3 class="text-center">"Show It, Snap It, Take it!"</h3>
                </div>
            </div>
        </div>
    </div>

@endsection

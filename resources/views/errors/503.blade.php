<!DOCTYPE html>
<html>
    <head>
        <title>Be right back.</title>

        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
              integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <style>
            html, body {
                height: 100%;
                background: #EAEAEA !important;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                color: #B0BEC5;
                display: table;
                font-weight: 100;
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 72px;
                margin-bottom: 10px;
            }
        </style>
    </head>
    <body>

        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="panel panel-default">
                        <div class="panel-heading" style="font-size:25px;">Welcome to  <span class="glyphicon glyphicon glyphicon-qrcode " aria-hidden="true"></span> QR-ATS</div>
                        <div class="panel-body">
                            <div class="title"> <span class="glyphicon glyphicon glyphicon-wrench" aria-hidden="true">  </span> ERROR 503!</div>
                            <div class="title" style="font-size: 50px;color: #2e3436;">Sorry we're down for maintenance!<br></div>
                            <div class="title" style="font-size: 40px;color: #2e3436;">We'll be back shortly!<br></div>
                            <br>
                            <div class="custom" style="font-size: 20px;color: #2e3436;">
                                <div class="title" style="font-size: 20px;color: #2e3436;">*Click button to visit Maintenance Control Page*</div>
                                <div class="title" style="font-size: 20px;color: #2e3436;">*Only Applicable for <b>Admin Role</b>*</div>

                                <a href="{{ url('/maintenance') }}" class="btn btn-primary  btn-lg" role="button">Maintenance Page</a>
                            </div>

                            <!-- Not working
                            @if(Auth::guest())
                                <div class="custom" style="font-size: 20px;color: #2e3436;">
                                    <div class="title" style="font-size: 30px;color: #2e3436;"><br>If you wish to Login, click button below!</div>
                                    <div class="title" style="font-size: 20px;color: #2e3436;">*Only applicable for admin*</div>
                                    <br>
                                    <a href="{{ url('/login') }}" class="btn btn-primary  btn-lg" role="button">Login</a>
                                </div>
                            @elseif(Auth::user()->user_type == 1 )
                                <div class="custom" style="font-size: 20px;color: #2e3436;">
                                    <div class="title" style="font-size: 30px;color: #2e3436;"><br>Only allowed to access Maintenance Control Page!</div>
                                    <div class="title" style="font-size: 20px;color: #2e3436;">*Click button to visit Maintenance Control Page*</div>
                                    <br>
                                    <a href="{{ url('/maintenance') }}" class="btn btn-primary  btn-lg" role="button">Login</a>
                                </div>
                            @endif
                            -->

                        </div>
                    </div>
                </div>
            </div>
            <p>© 2016 Asia Pacific University All Right Reserved <br>For more information, please contact developer via <a href="http://www.kavierkoo.com">kavierkoo.com</a>.</p>
        </div>

    </body>
</html>

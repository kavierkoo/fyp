<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class View_count extends Model
{
    protected $fillable =[ //Updatable by users without showing MassAssignemntException
        'view_name',
        'view_count',
        'created_at',
        'updated_at'
    ];
}

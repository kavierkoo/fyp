<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    protected $fillable =[ //Updatable by users without showing MassAssignemntException
        'module_code',
        'module_name',
        'intake_code',
        'user_email'
    ];
}

@extends('layouts.app')

<!-- This view will be handling both ADD INTAKE and ADD MODULE-->
@section('content')
    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
            <div class="panel-heading">Add new {{$add_id}}</div>
            <div class="panel-body ">
                <legend>Add new {{$add_id}}</legend>

                <!--Start of Form-->
            {!! Form::open(['class'=>'form-horizontal','url'=>$url]) !!}

            <!--Class Type-->
                <div class="form-group">
                    {!! Form::label($add_id.' Code ',null,['class'=>'col-sm-3 control-label']) !!}
                    <div class ="col-sm-9">
                        {!!Form::text($add_id.'_code',null, ['class'=>'form-control input-sm'])!!}
                    </div>
                </div>
                @if($add_id == 'module')
                    <div class="form-group">
                        {!! Form::label('Module Name ',null,['class'=>'col-sm-3 control-label']) !!}
                        <div class ="col-sm-9">
                            {!!Form::text('module_name',null, ['class'=>'form-control input-sm'])!!}
                        </div>
                    </div>
                @endif

                <!--Button-->
                <div class="text-center">
                    {!!  Form::submit('Create',['class'=>'btn btn-primary ']) !!}
                </div>

            {!! Form::close() !!} <!--End of Form-->

            </div>
        </div>
    </div>


@endsection

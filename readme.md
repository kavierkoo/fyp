#QR-ATS 
QR-Code Attendance Taking system aka QR-ATS is a system developed by Kavier Koo for his final year project to act as  an alternative method for attendance taking with the aim to maximize the effectiveness and efficiency of time consumption for attendance taking during in classes.

For more information, Please refer to the QR-ATS Documentation.

##System Information
Language    : PHP 

Framework   : Laravel version 5.3

CSS/Js		: Bootstrap version 3.3.7

QR Library	: SimpleSoftwareio - SimpleQrCode

Database    : PHPMyAdmin

#Sample username and password
Hit the "v" button on login page

##User Manual
Please refer to [User Manual](https://bitbucket.org/kavierkoo/fyp/src/4bfe3e2da2b87417d504b22fda1b3da0476a085d/User%20Manual.pdf?at=master&fileviewer=file-view-default)

##About Me
Please refer to http://www.kavierkoo.com for more information.

##License
Copyright © All Rights Reserved. Asia Pacific University
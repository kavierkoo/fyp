<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Qr extends Model
{
    protected $fillable =[ //Updatable by users without showing MassAssignemntException
        'class_date',
        'lecturer_email',
        'module_code',
        'intake_code',
        'class_type',
        'status',
    ];
}

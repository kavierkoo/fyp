<?php

namespace App\Http\Controllers;

use App\View_count;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $admin_count = View_count::where('view_name','welcome')->first();
        return view('home')->with('admin_count',$admin_count);
    }
}

@extends('layouts.app')

@section('content')


    <div class="panel panel-default">
        <div class="panel-heading">QR-ATS Information</div>
        <div class="panel-body"style="padding-top: 10px">

            <!-- Tab bar-->
            <ul class="nav nav-tabs">
                <li class="active"><a href="#users">Users</a></li>
                <li ><a href="#intakes">Intakes</a></li>
                <li ><a href="#modules">Modules</a></li>
            </ul>

            <!-- Tab's content-->
            <div class="tab-content">
                <div id="users" class="tab-pane fade in active">
                    <div class="well " style=" background: #FFFFFF; padding: 5px; margin-bottom: 5px;">
                        <legend style="padding-left: 5px;margin-top: 5px;margin-bottom: 5px">Users</legend>
                        @if(count($users) == 0)
                            <h4 class="text-center"style="padding: 10px">No User found!</h4>
                        @else
                            <h3 class="text-center" style="padding-left: 5px;margin: 0px;"><u>Total Users : {{count($users)}}</u></h3>
                            <table class="table table-striped  text-center "style="margin-bottom: 0px;">
                                <thead>
                                <tr>
                                    <th class="text-center">User Type</th>
                                    <th class="text-center">ID</th>
                                    <th class="text-center">Email</th>
                                    <th class="text-center">Name</th>
                                    <th class="text-center">Intake ID</th>
                                    <th class="text-center">Created At</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($users as $user)
                                    <tr>
                                        <td>
                                            @if($user->user_type==3)
                                                Student
                                            @elseif($user->user_type==2)
                                                Lecturer
                                            @elseif($user->id==1)
                                                Owner
                                            @elseif($user->user_type==1)
                                                Admin
                                            @else
                                                Error
                                            @endif
                                        </td>
                                        <td>{{$user->id}}
                                        </td>
                                        <td>
                                            @if($user->id==1 && Auth::user()->id !== 1)
                                                ******@*****.***
                                            @else
                                                {{$user->email}}
                                            @endif
                                        </td>
                                        <td>
                                            @if($user->id==1 && Auth::user()->id !== 1)
                                                ******
                                            @else
                                                {{$user->name}}
                                            @endif
                                        </td>
                                        <td>
                                            @if($user->intake_id == null && $user->user_type!==3)
                                                N/A
                                            @elseif($user->intake_id == null &&$user->user_type==3 )
                                                -
                                            @else
                                                {{$user->intake_id}}
                                            @endif
                                        </td>
                                        <td>
                                            @if($user->id==1 && Auth::user()->id !== 1)
                                                ****-**-** **:**:**
                                            @else
                                                {{$user->created_at}}
                                            @endif
                                        </td>

                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @endif
                    </div> <!-- End of Well -->
                </div>

                <!-- Intakes-->
                <div id="intakes" class="tab-pane fade ">
                    <div class="well " style=" background: #FFFFFF; padding: 5px; margin-bottom: 5px;">
                        <legend style="padding-left: 5px;margin-top: 5px;margin-bottom: 5px">Intakes</legend>
                        @if(count($intakes) == 0)
                            <h4 class="text-center" style="padding: 10px">No Intake found!</h4>
                        @else
                            <h3 class="text-center" style="padding-left: 5px;margin: 0px;"><u>Total Intakes: {{count($intakes)}}</u></h3>
                            <table class="table table-striped  text-center "style="margin-bottom: 0px;">
                                <thead>
                                <tr>
                                    <th class="text-center">ID</th>
                                    <th class="text-center">Intake Code</th>
                                    <th class="text-center">Created At</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($intakes as $intake)
                                    <tr>
                                        <td>{{$intake->id}}</td>
                                        <td>{{$intake->intake_code}}</td>
                                        <td>{{$intake->created_at}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @endif
                    </div> <!-- End of Well -->
                </div> <!-- end of Intakes-->

                <!-- Module -->
                <div id="modules" class="tab-pane fade ">
                    <div class="well " style=" background: #FFFFFF; padding: 5px; margin-bottom: 5px;">
                        <legend style="padding-left: 5px;margin-top: 5px;margin-bottom: 5px">Modules</legend>
                        @if(count($modules) == 0)
                            <h4 class="text-center" style="padding: 10px">No Module found!</h4>
                        @else
                            <h3 class="text-center" style="padding-left: 5px;margin: 0px;"><u>Total Modules : {{count($modules)}}</u></h3>
                            <table class="table table-striped  text-center "style="margin-bottom: 0px;">
                                <thead>
                                <tr>
                                    <th class="text-center">ID</th>
                                    <th class="text-center">Intake Code</th>
                                    <th class="text-center">Module Code</th>
                                    <th class="text-center">Module Name</th>
                                    <th class="text-center">Lecturer</th>
                                    <th class="text-center">Created At</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($modules as $module)
                                    <tr>
                                        <td>{{$module->id}}</td>
                                        <td>{{$module->intake_code}}</td>
                                        <td>{{$module->module_code}}</td>
                                        <td>{{$module->module_name}}</td>
                                        <td>{{$module->user_email}}</td>
                                        <td>{{$module->created_at}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @endif
                    </div> <!-- End of Well -->
                </div> <!-- end of Modules-->

            </div> <!-- end of Tab-content -->
        </div> <!-- end of <div class="panel-body">-->
    </div> <!-- end of <div class="panel panel-default">-->


@endsection

@section('footer')
    <script>
        $(document).ready(function(){
            $(".nav-tabs a").click(function(){
                $(this).tab('show');
            });
        });
    </script>

@endsection
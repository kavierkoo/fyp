<!-- Left Side Of Navbar -->
<ul class="nav navbar-nav">
    @yield('left_navbar')
</ul>

<!-- Right Side Of Navbar -->
<ul class="nav navbar-nav navbar-right">

    @yield('right_navbar')
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
            {{ Auth::user()->name }} <span class="caret"></span>
        </a>
        <ul class="dropdown-menu" role="menu">

            @yield('right_navbar_dropdown')
            <li class="dropdown-header"><strong>Student Role</strong></li>
            <li role="separator" class="divider"></li>


            <li><a href="{{ url('/logout') }}"onclick="event.preventDefault();
                                                document.getElementById('logout-form').submit();">Logout</a>
                <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </li>
        </ul> <!--End of dropdown menu-->
    </li> <!-- End of dropdown -->
    @endif
</ul> <!-- End of Nav Bar-->
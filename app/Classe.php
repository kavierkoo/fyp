<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Classe extends Model
{
    protected $fillable =[ //Updatable by users without showing MassAssignemntException
        'class_date',
        'class_type',
        'intake_code',
        'module_code',
        'user_email',
        'attendance',
        'qr_id',
    ];

    // Sample command to insert data instead of using Save
    /* $article = new App\Article
     * $intake->intake_code = 'UC3F1603IT(ISS)'
     * $intake->save();
     * OR
     * $article = App\Article::create(['title' => 'New Article', 'body' => 'New body']);
     */

    //Flow to update something
    /*  $article = App\Article:::find(2) // To find via ID
     *  $article->toArray(); // Save it to an array
     *  $article->body = 'Updated'; // Value to update
     *  $article->save(); //Save changes
     *  OR
     *  $article->update(['body' => 'Updated again']); //calling update method
     */
}

@extends('layouts.app')

@section('content')


        <div class="panel panel-default">
            <div class="panel-heading">Qr Code</div>
            <div class="panel-body">
                <div class="well col-md-4 text-center " style=" padding: 5px;margin-bottom: 5px;">
                    <h2>Scan me for attendance!</h2>
                    @if(isset($qrcontent))
                        <img src="data:image/png;base64, {!! base64_encode(QrCode::format('png')->merge('\public\img\logo.png',.15)->errorCorrection('H')->margin(0)->size(350)->generate($qrcontent)) !!} ">
                    @endif

                </div>

                <!--For decoration purpose-->


                <div class="well  col-xs-offset-4" style="padding: 5px; margin-left: 381px;margin-bottom: 5px; ">
                    <h2 style="padding-left: 5px;">Records</h2>

                    <div class="well" style=" background: #FFFFFF; padding: 5px; margin-bottom: 5px;">

                        <table class="table table-striped"style="margin-bottom: 0px;">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>E-mail</th>
                                <th>Intake</th>
                                <th>Status</th>
                            </tr>
                            </thead>
                            <tbody>

                            <tr>
                                <th scope="row">1</th>
                                <td>tp028601@test.com</td>
                                <td>UC3F1601IT(FC)</td>
                                <td>Present</td>
                            </tr>
                            <tr>
                                <th scope="row">2</th>
                                <td>tp028600@test.com</td>
                                <td>UC3F1601IT(ISS)</td>
                                <td>Present</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="form-group ">
                        <!--Start of Form-->
                        {!! Form::open(['class'=>'form-horizontal text-center','url'=>'/home']) !!}
                        <!-- Passing QR ID -->
                        @if(isset($qrid))
                            {!! Form::hidden('qrid', $qrid)!!}
                        @endif

                        <br>
                        {!!  Form::submit('Expired',['class'=>'btn btn-primary']) !!}

                        {!! Form::close() !!} <!--End of Form-->
                    </div>
             </div>
          </div>
        </div>

@endsection

@section('footer')
    <script>
        /*
        //call new page
        jQuery(document).ready(function () {
            newTab();
        });

        function newTab() {
            var form = document.createElement("form");
            form.method = "GET";
            form.action = "http://www.google.com";
            form.target = "_blank";
            document.body.appendChild(form);
            form.submit();
        }
        */
    </script>
@endsection
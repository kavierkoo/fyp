<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Intake extends Model
{
    protected $fillable =[ //Updatable by users without showing MassAssignemntException
        'intake_code'
    ];
}

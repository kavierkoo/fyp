<style>
    .custom
    {
        text-align: center;
        color: #122b40;
        font-weight: 100;
        font-family: 'Lato', sans-serif;
        font-size: 72px;
    }
</style>
@extends('layouts.app')

@section('title')
    Error 498
@endsection

<!-- Inside Container -->
@section('content')


    <div class="custom" style=" background-color: #ce8483;"> <span class="glyphicon glyphicon glyphicon-alert" aria-hidden="true">  </span> ERROR 498! <br></div>
    <br>
    <div class="custom" style="font-size: 50px;color: #2e3436;">Oops! Session expired!</div>
    <div class="custom" style="font-size: 30px;color: #2e3436;"><br>You are required to send the request again!</div>
    <div class="custom" style="font-size: 25px;color: #2e3436;">
        If you wish to go back to Home page, click button below!<br><br>
        <a href="{{ url('/login') }}" class="btn btn-primary  btn-lg" role="button">Home</a>
    </div>


@endsection


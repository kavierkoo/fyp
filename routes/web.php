<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/


use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;


// Default
Route::get('/', 'WelcomeController@welcome');

Auth::routes();
Route::get('/home', 'HomeController@index');

// Admin Controller
    // Maintenance Function
Route::get('/maintenance','AdminController@maintenance');
Route::post('/maintenance','AdminController@change_maintenance');
    // Add INTAKE and MODULE Function
Route::get('/add_intake','AdminController@add_intake'); // Recycle view
Route::get('/add_module','AdminController@add_module'); // Recycle view
Route::post('/add_intake','AdminController@save_intake');// Recycle view
Route::post('/add_module','AdminController@save_module');// Recycle views
    // Link Function
Route::get('/link_module_intake','AdminController@link_intake'); // Recycle view
Route::get('/link_module_lecturer','AdminController@link_lecturer'); // Recycle view
Route::get('/link_intake_student','AdminController@link_student'); // Recycle view
Route::post('/link_module_intake','AdminController@save_link_intake');
Route::post('/link_module_lecturer','AdminController@save_link_lecturer');
Route::post('/link_intake_student','AdminController@save_link_student');
    // Reports
Route::get('/admin_reports','AdminController@generatedQR');
Route::get('/info','AdminController@info');

// Lecturer Controller
Route::get('/showqr','LecturerController@showqr');
Route::get('/showqr_result','LecturerController@showqr_result');
Route::get('/generate_qr','LecturerController@generate_qr');
Route::post('/showqr','LecturerController@receive');
Route::post('/home','LecturerController@expired');
    // Reports
    Route::get('/lecturer_reports','LecturerController@lecturer_reports');

// Student Controller
Route::get('/student_attendance','StudentController@show_attendance');

// custom_guest
Route::get('/confirm','custom_guest@confirm');
Route::post('/confirm','custom_guest@store_confirm');

//Testing purposes
Route::get('/test', function () {
    return view('justfortesting');
});

Route::get('/498', function () {
    return view('errors.498');
});

Route::get('/503', function () {
    return view('errors.503');
});

Route::get('/404', function () {
    return view('errors.404');
});

Route::get('/403', function () {
    return view('errors.403');
});
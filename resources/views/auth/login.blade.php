@extends('layouts.app')

@section('content')

<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
            <div class="panel-heading">
                Login | To Display Sample Usernames & Passwords, Click >>

                <!-- Hide and Show bar-->
                <button type="button" onclick="showbtn()" class="btn btn-default btn-xs " id="link_show" aria-label="Center Align">
                    <span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span>
                </button>
                <button type="button" onclick="hidebtn()" class="btn btn-default btn-xs hidden" id="link_hide" aria-label="Center Align">
                    <span class="glyphicon glyphicon-chevron-up" aria-hidden="true"></span>
                </button>
                <<
            </div>


            <div id="link_hide_show" class="hidden">
                <div class="well " style=" background: #FFFFFF; padding: 5px; margin-bottom: 5px;">
                    <table class="table table-bordered text-center"style="margin-bottom: 0px;">
                        <thead>
                        <tr>
                            <th class="text-center">User Type</th>
                            <th class="text-center">Username</th>
                            <th class="text-center">Password</th>
                        </tr>
                        </thead>
                        <tbody>
                           <tr>
                               <td>Admin</td>
                               <td>admin@qrats.com</td>
                               <td rowspan="9" style="text-align: center; vertical-align: middle;">testtest</td>
                           </tr>
                           <tr>
                               <td rowspan="2" style="text-align: center; vertical-align: middle;">Lecturer</td>
                               <td>lecturer1@qrats.com</td>
                           </tr>
                           <tr>
                               <td>lecturer2@qrats.com</td>
                           </tr>
                           <tr>
                               <td rowspan="6" style="text-align: center; vertical-align: middle;">Student</td>
                               <td>student1@qrats.com</td>
                           </tr>
                           <tr>
                               <td>student2@qrats.com</td>
                           </tr>
                           <tr>
                               <td>student3@qrats.com</td>
                           </tr>
                           <tr>
                               <td>student4@qrats.com</td>
                           </tr>
                           <tr>
                               <td>student5@qrats.com</td>
                           </tr>
                           <tr>
                               <td>student6@qrats.com</td>
                           </tr>
                        </tbody>
                    </table>
                </div> <!-- End of Well -->
            </div>

            <div class="panel-body">
                <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                    {{ csrf_field() }}

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                        <div class="col-md-6">
                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <label for="password" class="col-md-4 control-label">Password</label>

                        <div class="col-md-6">
                            <input id="password" type="password" class="form-control" name="password" required>

                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="remember"> Remember Me
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-8 col-md-offset-4">
                            <button type="submit" class="btn btn-primary">
                                Login
                            </button>

                            <a class="btn btn-link" href="{{ url('/password/reset') }}">
                                Forgot Your Password?
                            </a>
                        </div>
                    </div>
                    <!--<p class="text-center">For sample username and password, please contact developer via <a href="http://www.kavierkoo.com/Info">kavierkoo.com</a>!</p>-->
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@section('footer')
    <script>
        function hidebtn() {
            $("#link_hide_show").addClass('hidden');
            $("#link_hide").addClass('hidden');
            $("#link_show").removeClass('hidden');
        }
        function showbtn() {
            $("#link_hide_show").removeClass('hidden');
            $("#link_hide").removeClass('hidden');
            $("#link_show").addClass('hidden');
        }
    </script>
@endsection
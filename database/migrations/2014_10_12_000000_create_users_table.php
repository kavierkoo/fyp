<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {

            $table->increments('id');


            //Login Credential
            $table->string('email')->unique();
            $table->string('password');
            $table->string('name');

            // NULL Variable
            $table->string('module_id');
            $table->string('intake_id');

            //Admin,student,lecturer
            $table->integer('user_type');

            //For security purpose
            $table->rememberToken();
            $table->timestamps();

            /** Default Setting
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
            */
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}

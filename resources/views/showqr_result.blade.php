@extends('layouts.app')

@section('content')


    <div class="panel panel-default">
        <div class="panel-heading">Qr Code</div>
        <div class="panel-body">
            <!--For decoration purpose-->
                <h2 style="padding-left: 5px;">Records</h2>

                <div class="well" style=" background: #FFFFFF; padding: 5px; margin-bottom: 5px;">

                    <table class="table table-striped"style="margin-bottom: 0px;">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>E-mail</th>
                            <th>Intake</th>
                            <th>Status</th>
                        </tr>
                        </thead>
                        <tbody>

                        <tr>
                            <th scope="row">1</th>
                            <td>Mark</td>
                            <td>Otto</td>
                            <td>@mdo</td>
                        </tr>
                        <tr>
                            <th scope="row">1</th>
                            <td>Mark</td>
                            <td>Otto</td>
                            <td>@mdo</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="form-group ">
                    <!--Start of Form-->
                {!! Form::open(['class'=>'form-horizontal text-center','url'=>'/']) !!}
                <!-- Passing QR ID -->

                    <br>
                {!!  Form::submit('Expired',['class'=>'btn btn-primary']) !!}

                {!! Form::close() !!} <!--End of Form-->
                </div>
            </div>
        </div>
    </div>

@endsection

@section('footer')

@endsection
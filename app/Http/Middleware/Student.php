<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class Student
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check() == 1) { //check if singed in
            if (Auth::user()->user_type == '3') // is an student
            {
                return $next($request); // pass the student
            } else {
                abort(403);
            }
        }
        else
        {
            return redirect('login')->with('flash_error', 'Oops! Login is required!');
        }
    }
}

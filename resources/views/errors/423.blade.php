<style>
    .custom
    {
        text-align: center;
        color: #D8000C;
        font-weight: 100;
        font-family: 'Lato', sans-serif;
        font-size: 72px;
    }
</style>
@extends('layouts.app')

@section('title')
    Error 423
@endsection

<!-- Inside Container -->
@section('content')


    <div class="custom" style=" background-color: #FFBABA;"> <span class="glyphicon glyphicon glyphicon-alert" aria-hidden="true">  </span> ERROR 423! <br></div>
    <br>
    <div class="custom" style="font-size: 50px;color: #2e3436;">Oops! Content or URL is LOCKED!</div>
    <div class="custom" style="font-size: 30px;color: #2e3436;"><br>The QR code or URL is Expired from QR-ATS database!<br>Please Contact your respected lecturer to reactivate it!</div><br>
    <div class="custom" style="font-size: 25px;color: #2e3436;">
        If you wish to go back to Home page, click button below!<br><br>
        <a href="{{ url('/login') }}" class="btn btn-primary  btn-lg" role="button">Home</a>
    </div>


@endsection


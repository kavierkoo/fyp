@extends('layouts.app')

@section('content')
    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
            <div class="panel-heading">Link Intake To Student</div>
            <div class="panel-body ">
                <legend>Link Intake To Student</legend>

                <!--Start of Form-->
                {!! Form::open(['class'=>'form-horizontal','url'=>'/showqr']) !!}



                <!--Class Type-->
                <div class="form-group">
                    {!! Form::label('Intake Code ',null,['class'=>'col-sm-2 control-label']) !!}
                    <div class ="col-sm-10">
                        {!!Form::select('class_type',['lecturer'=>'Lecturer','tutorial'=>'Tutorial'], null,['placeholder' => '','class'=>'form-control'])!!}
                    </div>
                </div>
                <!--Class Type-->
                <div class="form-group">
                    {!! Form::label('Student',null,['class'=>'col-sm-2 control-label']) !!}
                    <div class ="col-sm-10">
                        {!!Form::select('class_type',['lecturer'=>'Lecturer','tutorial'=>'Tutorial'], null,['placeholder' => '','class'=>'form-control'])!!}
                    </div>
                </div>



                    <!--Button-->
                    <div class="text-center">
                        {!!  Form::submit('Link',['class'=>'btn btn-primary ']) !!}
                    </div>

                {!! Form::close() !!} <!--End of Form-->

            </div>
        </div>
    </div>


@endsection

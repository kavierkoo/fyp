@extends('layouts.app')

@section('content')
    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
            <div class="panel-heading">Confirmation</div>
            <div class="panel-body ">
                <div class="well table-responsive" style="padding:10px">
                    <legend class="text-center">Class Details</legend>
                    <table class="table text-left" style="font-size: 11px">
                       <tr>
                           <td class="text-right"><strong>Class Date :</strong></td>
                           <td><strong>{{$class_date}}</strong></td>
                       </tr>
                        <tr>
                           <td class="text-right"><strong>Lecturer Email :</strong></td>
                           <td><strong>{{$lec_email}}</strong></td>

                       </tr>
                        <tr>
                            <td class="text-right"><strong>Intake Code :</strong></td>
                            <td><strong>
                                    @foreach($intake as $intakes)
                                        {{$intakes}}
                                        <br>
                                    @endforeach
                                </strong></td>
                            </tr>
                        <tr>
                            <td class="text-right"><strong>Module Code :</strong></td>
                            <td><strong>{{$module}}</strong></td>
                        </tr>
                        <tr>
                            <td class="text-right"><strong>Class Type :</strong></td>
                            <td><strong>{{$class_type}}</strong></td>
                        <tr>
                        </tr>
                            <td class="text-right "><strong>QR Status :</strong></td>
                            <td><strong>{{$qr_status}}</strong></td>
                        </tr>
                    </table>

                </div>

                <div class="well text-center" style="padding:10px">
                    <legend >Login & Confirm </legend>
                <!--Start of Form-->
                    {!! Form::open(['class'=>'form-horizontal','url'=>'/confirm']) !!}
                    <!--Login ID-->
                    <div class="form-group form-group-sm">
                        {!! Form::label('E-Mail',null,['class'=>'col-xs-4 control-label']) !!}
                        <div class ="col-xs-8">
                            {!!Form::text('email',null, ['class'=>'form-control input-sm'])!!}
                        </div>
                    </div>

                    <!--Login Password-->
                    <div class="form-group form-group-sm ">
                        {!! Form::label('Password',null,['class'=>'col-xs-4 control-label']) !!}
                        <div class ="col-xs-8">
                            {!! Form::password('password', ['class' => 'form-control input-sm'])!!}
                        </div>
                    </div>

                    <!--Button-->
                    <div class="text-center">
                        {!!  Form::submit('Login & Confirm' ,['class'=>'btn btn-primary  ']) !!}
                    </div>

                    <!-- Passing QR ID -->
                    {!! Form::hidden('qr_id', $qr_id)!!}


                    {!! Form::close() !!} <!--End of Form-->
                    </div>
            </div>
        </div>
    </div>


@endsection

@extends('layouts.app')

@section('content')


    <div class="panel panel-default">
        <div class="panel-heading">Attendance Records</div>
        <div class="panel-body"style="padding-top: 10px">

            <!-- Tab bar-->
            <ul class="nav nav-tabs">
                <li class="active"><a href="#qr_history">Generated QR Records</a></li>
                <li ><a href="#summary">Class Attendances</a></li>
            </ul>

            <!-- Tab's content-->
            <div class="tab-content">
                <div id="summary" class="tab-pane fade ">
                    <div class="well " style=" background: #FFFFFF; padding: 5px; margin-bottom: 5px;">
                        <legend style="padding-left: 5px;margin-top: 5px;margin-bottom: 5px">Class Attendances</legend>
                        @if(count($attendance_records) == 0)
                            <h4 class="text-center"style="padding: 10px">No History!</h4>
                        @else
                            @foreach($modules as $module)
                                <table class="table table-striped  text-center "style="margin-bottom: 0px;">
                                    <h4 >Module Code : {{$module->module_code}}</h4>
                                    <thead>
                                        <tr>
                                            <th class="text-center">Student</th>
                                            <th class="text-center">QR ID</th>
                                            <th class="text-center">Class Date</th>
                                            <th class="text-center">Intake Code</th>
                                            <th class="text-center">Module Code</th>
                                            <th class="text-center">Class Type</th>
                                            <th class="text-center">Status</th>
                                            <th class="text-center">Recorded At</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($attendance_records as $attendance_record)
                                            @if(count($attendance_record)==0)
                                                <h4 class="text-center"style="padding: 10px">No History!</h4>
                                            @elseif($module->module_code==$attendance_record->module_code)
                                                <tr>
                                                    <td>{{$attendance_record->user_email}}</td>
                                                    <td>{{$attendance_record->qr_id}}</td>
                                                    <td>{{$attendance_record->class_date}}</td>
                                                    <td>{{$attendance_record->module_code}}</td>
                                                    <td>{{$attendance_record->intake_code}}</td>

                                                    <td>{{$attendance_record->class_type}}</td>
                                                    <td>{{$attendance_record->attendance}}</td>
                                                    <td>{{$attendance_record->created_at}}</td>
                                               </tr>
                                            @endif
                                        @endforeach
                                    </tbody>
                                </table>
                                @if(strpos($attendance_record->module_code,$module->module_code)!==true)
                                    <h5 class="text-center"style="padding: 10px">--End of Module Attendance!--</h5>
                                @endif
                            @endforeach
                        @endif

                    </div> <!-- End of Well -->

                </div>
                <!-- Scanned QR Records-->
                <div id="qr_history" class="tab-pane fade in active">
                    <div class="well " style=" background: #FFFFFF; padding: 5px; margin-bottom: 5px;">
                        <legend style="padding-left: 5px;margin-top: 5px;margin-bottom: 5px">Generated QR Records</legend>
                        @if(count($qr_records) == 0)
                            <h4 class="text-center" style="padding: 10px">No Generated QR History!</h4>
                        @else
                            <h3 class="text-center" style="padding-left: 5px;margin: 0px;"><u>Total Generated QR : {{count($qr_records)}}</u></h3>
                            <table class="table table-striped  text-center "style="margin-bottom: 0px;">
                                <thead>
                                <tr>
                                    <th class="text-center">Qr ID</th>
                                    <th class="text-center">Class Date</th>
                                    <th class="text-center">Intake Code</th>
                                    <th class="text-center">Module Code</th>
                                    <th class="text-center">Class Type</th>
                                    <th class="text-center">Status</th>
                                    <th class="text-center">Created At</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($qr_records as $qr_record)
                                    <tr>
                                        <td>{{$qr_record->id}}</td>
                                        <td>{{$qr_record->class_date}}</td>
                                        <td>{{$qr_record->intake_code}}</td>
                                        <td>{{$qr_record->module_code}}</td>
                                        <td>{{$qr_record->class_type}}</td>
                                        <td>{{$qr_record->status}}</td>
                                        <td>{{$qr_record->created_at}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @endif
                    </div> <!-- End of Well -->
                </div> <!-- end of QR_History-->
            </div> <!-- end of Tab-content -->
        </div> <!-- end of <div class="panel-body">-->
    </div> <!-- end of <div class="panel panel-default">-->


@endsection

@section('footer')
    <script>
        $(document).ready(function(){
            $(".nav-tabs a").click(function(){
                $(this).tab('show');
            });
        });
    </script>

@endsection
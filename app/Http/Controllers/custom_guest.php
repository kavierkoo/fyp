<?php

namespace App\Http\Controllers;

use App\Classe;
use App\user;
use App\Qr;
use Illuminate\Support\Facades\Auth;
use Request;
use Illuminate\Support\Facades\Input;


class custom_guest extends Controller
{
    public function confirm()
    {
        // get QR id from URL
        $qr_id = input::get('id');

        //check QR Status if expired or Active
        $qr_status = Qr::select('status as ""')->where('id','=',$qr_id)->get();
        $qr_status =  str_replace(str_split('{}[]\\/:*?"<>|'),'',$qr_status);

        // Retrieve QR info
        $class_date = str_replace(str_split('{}[]\\/:*?"<>|'),'',(Qr::select('class_date as ""')->where('id','=',$qr_id)->get()));
        $lec_email = str_replace(str_split('{}[]\\/:*?"<>|'),'',(Qr::select('lecturer_email as ""')->where('id','=',$qr_id)->get()));
        $module =  str_replace(str_split('{}[]\\/:*?"<>|'),'',(Qr::select('module_code as ""')->where('id','=',$qr_id)->get()));
        $intake = str_replace(str_split('{}[]\\/:*?"<>|'),'',(explode(',',(Qr::select('intake_code as ""')->where('id','=',$qr_id)->get()))));
        $class_type = str_replace(str_split('{}[]\\/:*?"<>|'),'',(Qr::select('class_type as ""')->where('id','=',$qr_id)->get()));

        // QR ID NOT NUL, QR is within database FIRST AND LAST RANGE
        if ($qr_id !== null && (Qr::find($qr_id)!==null)) {
            if ($qr_status == "active"){ //check if QR code expired or active
                $qr_status = "Active";
                return view('/confirm', compact('qr_id','class_date','qr_status','lec_email','module','intake','class_type'));
            }
            else{
                abort(423); // LOCKED
            }

        } else {
            abort(406); // NOT ACCEPTABLE
        }
    }

    public function store_confirm(\Illuminate\Http\Request $request){
        // validator
        $this ->validate($request,[
            'email' => 'required|email',
            'password' => 'required',
        ]);

        // fetch data from form
        $email=Request::input('email');
        $password=Request::input('password');

        // Authenticate user
        if(Auth::attempt(['email' => $email, 'password' => $password])){
            if(Auth::user()->user_type == 3 ){ // if is student
                //check if student in the intake
                // grab qr_id
                $qr_id=Request::input('qr_id');

                // grab student_email
                $student_email= Auth::user()->email;

                // grab date
                $qr_date =  str_replace(str_split('{}[]\\/:*?"<>|'),'',(Qr::select('class_date as ""')->where('id','=',$qr_id)->get()));

                // grab type
                $class_type =  str_replace(str_split('{}[]\\/:*?"<>|'),'',(Qr::select('class_type as ""')->where('id','=',$qr_id)->get()));

                // grab module
                $module =  str_replace(str_split('{}[]\\/:*?"<>|'),'',(Qr::select('module_code as ""')->where('id','=',$qr_id)->get()));

                // grab intake
                $intake = str_replace(str_split('{}[]\\/:*?"<>|'),'',(Qr::select('intake_code as ""')->where('id','=',$qr_id)->get()));

                // crosscheck
                $check_intake = str_replace(str_split('{}[]\\/:*?"<>|'),'',(User::select('intake_id as ""')->where('email','=',$student_email)->get()));



                // If student with Correct Intake
                if(strpos($intake,$check_intake)!== false){

                    // Retrieve any previous record
                    $check_duplicate = Classe::where([
                        'user_email' => $student_email,
                        'qr_id' => $qr_id
                    ])->get();

                    // grab student only intake
                    $student_intake = str_replace(str_split('{}[]\\/:*?"<>|'),'',(User::select('intake_id as ""')->where('email','=',$student_email)->get()));

                    // Check if duplicate record
                    if($check_duplicate->isEmpty()){
                        // Save to table Classe
                        Classe::create([
                            'class_date' => $qr_date,
                            'class_type' => $class_type,
                            'intake_code' => $student_intake,
                            'module_code' => $module,
                            'user_email' => $student_email,
                            'attendance' => 'Present',
                            'qr_id' => $qr_id
                        ]);
                        \Session::flash('flash_success', 'Your attendance has been recorded!');
                        return view('/home');
                    }
                    else{
                        // Dont save return error
                        return back()->withErrors('Your attendance already recorded!');
                    }

                }
                else{

                    Auth::logout(); // Dont remain login
                    return back()->withErrors('Student not found in listed intake! Please try again!');
                }
            }
            else{ // if not student
                return back()->withErrors("Wrong User Role! Only Students Role is available!");
            }
        }
        else{
            return back()->withErrors("Incorrect user email or passwords!");
        }
    }


}

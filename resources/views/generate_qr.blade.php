@extends('layouts.app')

@section('content')
    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
            <div class="panel-heading">Generate QR Code</div>
            <div class="panel-body ">
                <legend>Class Details</legend>

                <!--Start of Form-->
                {!! Form::open(['class'=>'form-horizontal','url'=>'/showqr']) !!}

                <!--Date-->
                    <div class="form-group">
                        {!! Form::label('Class Date :',null,['class'=>'col-sm-2 control-label']) !!}
                        <div class ="col-sm-10"> {!! Form::date('Class_Date',date('Y-m-d'),['class'=>'form-control']) !!} </div>
                    </div>

                    <!--Module-->
                    <div class="form-group">
                        {!! Form::label('Module :',null,['class'=>'col-sm-2 control-label']) !!}
                        <div class ="col-sm-10">
                            {!!Form::select('module',$module_code, null,['placeholder' => 'Choose Module','class'=>'form-control'])!!}
                        </div>
                    </div>


                    <!--Intake-->
                    <div class="form-group">
                        {!! Form::label('Intake :',null,['class'=>'col-sm-2 control-label']) !!}
                        <div class="col-sm-10">

                            @foreach(array_unique($intake) as $intakes)
                                {!! Form::checkbox('intake[]', $intakes)!!} {{$intakes}}
                                <br>
                            @endforeach

                        </div>
                    </div>

                    <!--Class Type-->
                    <div class="form-group">
                        {!! Form::label('Class Type :',null,['class'=>'col-sm-2 control-label']) !!}
                        <div class ="col-sm-10">
                            {!!Form::select('class_type',['lecturer'=>'Lecturer','tutorial'=>'Tutorial'], null,['placeholder' => 'Choose Class Type','class'=>'form-control'])!!}
                        </div>
                    </div>

                    <!--Button-->
                    <div class="text-center">
                        {!!  Form::submit('Generate QR',['class'=>'btn btn-primary ']) !!}
                    </div>

                {!! Form::close() !!} <!--End of Form-->

            </div>
        </div>
    </div>


@endsection

<?php

namespace App\Http\Controllers;

use App\Classe;
use App\Module;
use App\Qr;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Collection;

class StudentController extends Controller
{
    public function __construct()
    {
        $this->middleware('student');
    }
    public function index()
    {
        return view('/');
    }

    public function show_attendance(){
        // QR History Records
        $attendance_records= null;
        $qr_records= Classe::select('class_date','class_type','module_code','attendance','intake_code')->where('user_email','=',Auth::user()->email)->get();
        $student_modules = null;
        $student_modules1 = null;
        // Share Variable
        $student_email = Auth::user()->email;
        $student_intake = Auth::user()->intake_id;


        //Variable for module
        // $student_modules
        // Grab module by filtering through Intakes
        $all_intakes = Module::select('module_code','intake_code')->get();

        // If student have no intake
        if($student_intake==null)
        {
            $status = 0;

            return view('/student_attendance',compact('status'));
        }
        else
        {
            $status = 1; //student have no intake

            //Grab module
            // Foreach filter
            foreach ($all_intakes as $all_intake){
                $student_modules=Module::where('intake_code','LIKE', '%'.$student_intake.'%')->get();


                /*
                // Abandon Inefficient Code
                $temp_intake = $all_intake ->intake_code;
                // If not combined Intake
                if($temp_intake == $student_intake){
                    $student_modules=Module::select('module_code')->where('intake_code',$temp_intake)->get();
                }

                // If combined Intake
                elseif(strpos($temp_intake,$student_intake)!== false){
                    $array_intakes= explode(',',$temp_intake);

                    // Search each exploded Intake
                    foreach ($array_intakes as $array_intake){

                        // If Correct Intake
                        if($array_intake == $student_intake){
                            $student_modules1=Module::select('module_code')->where('intake_code',$temp_intake)->get();
                        }
                    }
                }
                */
            }

            // Attendance for each module
            foreach ($student_modules as $student_module){
                //Clear Junkie data
                $check_attendances=[];
                $present_classes =0;
                $total_classes =0;
                $percent=0;

                // Grab QR
                $all_qrs = Qr::all();

                // count total classes and present class
                foreach ($all_qrs as $all_qr) {

                    $qr_id = $all_qr->id;

                    // Validate Intake
                    if ($student_intake == $all_qr->intake_code
                        || (strpos($all_qr->intake_code, $student_intake) !== false)) {

                        // Validate Module
                        if($student_module->module_code == $all_qr->module_code ) {

                            $total_classes = $total_classes + 1;

                            $match = [
                                'qr_id' => $qr_id,
                                'module_code' => $student_module->module_code,
                                'user_email' => $student_email,
                            ];

                            //present class
                            if(Classe::where($match)->exists()) {
                                $check_attendances[] = Classe::where($match)->get();
                            }
                        }
                    }
                }

                // Calculate Percent
                if($total_classes !== null || $present_classes!== null ){
                    $present_classes = count($check_attendances);

                    // Divided by 0 validation
                    if($present_classes == 0)
                    {
                        $percent = 0;
                    }
                    else
                    {
                        $percent = $present_classes / $total_classes * 100;
                    }
                }
                else{
                    $present_classes =0;
                    $total_classes =0;
                    $percent=0;
                }

                // Add to array
                $attendance_records[] =array(
                    'module_code' => $student_module->module_code,
                    'present' => $present_classes,
                    'total'=>$total_classes,
                    'percent'=>$percent,

                );

            }
            return view('/student_attendance',compact('qr_records','status','attendance_records'));
        }
    }
}

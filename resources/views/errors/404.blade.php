<style>
    .custom
    {
        text-align: center;
        color: #D8000C;
        font-weight: 100;
        font-family: 'Lato', sans-serif;
        font-size: 72px;
    }
</style>
@extends('layouts.app')

@section('title')
    Error 404
@endsection

<!-- Inside Container -->
@section('content')


    <div class="custom" style=" background-color: #FFBABA;"> <span class="glyphicon glyphicon glyphicon-alert" aria-hidden="true">  </span> ERROR 404! <br></div>
    <br>
    <div class="custom" style="font-size: 50px;color: #2e3436;">Oops! We can't find the page you're looking for!</div>
    <div class="custom" style="font-size: 30px;color: #2e3436;"><br>Make sure you typed the correct URL or Check Route!</div><br>
    <div class="custom" style="font-size: 25px;color: #2e3436;">
        If you wish to go back to Home page, click button below!<br><br>
        <a href="{{ url('/login') }}" class="btn btn-primary  btn-lg" role="button">Home</a>
    </div>


@endsection


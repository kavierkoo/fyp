@extends('layouts.app')

@section('content')
    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
            <div class="panel-heading">Maintenance Page</div>
            <div class="panel-body ">

                {!! Form::open(['class'=>'form','url'=>'/maintenance']) !!}

                @if (App::isDownForMaintenance())
                    <legend class="text-center">Current Application Status : <div style="color: #ff0000"><b>Maintenance Mode</b></div></legend>
                    <div class="form-group">
                        {!! Form::label('Maintenance :',null,['class'=>'col-sm-2 col-sm-offset-2 control-label','style'=>'padding-top:6px;padding-left:0px;']) !!}
                        <div class =" col-sm-offset-4">
                            {!!Form::select('status',['0'=>'Choose Action','Activate'=>'Deactivate Maintenance Mode'], null,['class'=>'form-control','style'=>'padding-top:0px;padding-bottom:0px;padding-left:6px;'])!!}
                        </div>
                    </div>
                @else
                    <legend class="text-center">Current Application Status : <div style="color: #2ab27b"><b>Live</b></div></legend>
                    <div class="form-group">
                        {!! Form::label('Maintenance :',null,['class'=>'col-sm-2 col-sm-offset-2 control-label','style'=>'padding-top:6px;padding-left:0px;']) !!}
                        <div class =" col-sm-offset-4">
                            {!!Form::select('status',['0'=>'Choose Action','Deactivate'=>'Activate Maintenance Mode'], null,['class'=>'form-control','style'=>'padding-top:0px;padding-bottom:0px;padding-left:6px;'])!!}
                        </div>
                    </div>
                @endif

                <!--Button-->
                <div class="text-center">
                    {!!  Form::submit('Execute',['class'=>'btn btn-primary ']) !!}
                </div>
            {!! Form::close() !!} <!--End of Form-->

            </div>
        </div>
    </div>
@endsection

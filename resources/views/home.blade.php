@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Welcome to QR-ATS</div>
                <div class="panel-body">

                    @if (App::isDownForMaintenance()&&Auth::user()->user_type == 1 )
                        Hello {{ Auth::user()->name }}, You are logged in as admin role!
                        <br><br>
                        <div class="text-center">
                            <div class="title" style="font-size: 40px;color: #2e3436;">Maintenance Mode!</div>
                            <div class="custom" style="font-size: 20px;color: #2e3436;">
                                <div class="title" style="font-size: 30px;color: #2e3436;">*Only allowed to access Maintenance Control Page!*</div>
                            </div>
                        </div>

                    @else
                        @if (App::isDownForMaintenance())
                            <div class="text-center">
                                <div class="title" style="font-size: 40px;color: #2e3436;">Sorry we're down for maintenance!<br></div>
                                <div class="title" style="font-size: 30px;color: #2e3436;">We'll be back shortly!<br></div>
                            </div>
                        @else
                            @if(Auth::user()->user_type == 1 )
                                Hello {{ Auth::user()->name }}, You are logged in as admin role!
                            @elseif(Auth::user()->user_type == 2 )
                                Hello {{ Auth::user()->name }}, You are logged in as lecturer role!
                            @elseif(Auth::user()->user_type == 3 )
                                Hello {{ Auth::user()->name }}, You are logged in as student role!
                            @endif
                        @endif
                    @endif
                    @if(Auth::user()->user_type == 1 )
						 @if(isset($admin_count))
							 <br><br>
							 <p class="text-center">
								QR-ATS has been visited {{$admin_count->view_count}} time by visitors!
								<br>
								Latest visit at {{$admin_count->updated_at}}
							 </p>
						@endif
					@endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

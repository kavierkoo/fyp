-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 20, 2017 at 11:20 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `fyp`
--
CREATE DATABASE IF NOT EXISTS `fyp` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `fyp`;

-- --------------------------------------------------------

--
-- Table structure for table `classes`
--

DROP TABLE IF EXISTS `classes`;
CREATE TABLE `classes` (
  `id` int(10) UNSIGNED NOT NULL,
  `class_date` date NOT NULL,
  `class_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `intake_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `module_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `attendance` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `qr_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `intakes`
--

DROP TABLE IF EXISTS `intakes`;
CREATE TABLE `intakes` (
  `id` int(10) UNSIGNED NOT NULL,
  `intake_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `intakes`
--

INSERT INTO `intakes` (`id`, `intake_code`, `created_at`, `updated_at`) VALUES
(1, 'intake_code_1', '2017-02-20 02:08:03', '2017-02-20 02:08:03'),
(2, 'intake_code_2', '2017-02-20 02:08:06', '2017-02-20 02:08:06'),
(3, 'intake_code_3', '2017-02-20 02:08:09', '2017-02-20 02:08:09');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_12_20_174736_create_intakes_table', 2),
(4, '2016_12_20_175028_create_modules_table', 2),
(5, '2016_12_20_181007_create_classes_table', 2),
(6, '2017_01_05_070252_add_qr_to_classes', 3),
(8, '2017_01_05_071122_create_qr_table', 4),
(9, '2017_01_06_145108_drop_table_users_moduleid', 5);

-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

DROP TABLE IF EXISTS `modules`;
CREATE TABLE `modules` (
  `id` int(10) UNSIGNED NOT NULL,
  `module_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `module_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `intake_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `modules`
--

INSERT INTO `modules` (`id`, `module_code`, `module_name`, `intake_code`, `user_email`, `created_at`, `updated_at`) VALUES
(1, 'Module_code_1', 'Module 1', 'intake_code_1', 'lecturer1@qrats.com', '2017-02-20 02:08:25', '2017-02-20 02:11:43'),
(2, 'Module_code_2', 'Module 2', 'intake_code_2', 'lecturer1@qrats.com', '2017-02-20 02:08:34', '2017-02-20 02:11:46'),
(3, 'Module_code_3', 'Module 3', 'intake_code_3', 'lecturer1@qrats.com', '2017-02-20 02:08:40', '2017-02-20 02:11:49'),
(4, 'Module_code_4', 'Module 4', 'intake_code_1,intake_code_2', 'lecturer1@qrats.com', '2017-02-20 02:08:45', '2017-02-20 02:11:52'),
(5, 'Module_code_5', 'Module 5', 'intake_code_2,intake_code_3', 'lecturer1@qrats.com', '2017-02-20 02:08:55', '2017-02-20 02:11:55'),
(6, 'Module_code_6', 'Module 6', 'intake_code_1,intake_code_3', 'lecturer2@qrats.com', '2017-02-20 02:09:16', '2017-02-20 02:11:59'),
(7, 'Module_code_7', 'Module 7', 'intake_code_1,intake_code_2,intake_code_3', 'lecturer2@qrats.com', '2017-02-20 02:09:22', '2017-02-20 02:12:03'),
(8, 'Module_code_8', 'Module 8', 'intake_code_1', 'lecturer2@qrats.com', '2017-02-20 02:09:33', '2017-02-20 02:12:07'),
(9, 'Module_code_9', 'Module 9', 'intake_code_2', 'lecturer1@qrats.com', '2017-02-20 02:09:59', '2017-02-20 02:12:10'),
(10, 'Module_code_10', 'Module 10', 'intake_code_3', 'lecturer2@qrats.com', '2017-02-20 02:10:06', '2017-02-20 02:12:14');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `qrs`
--

DROP TABLE IF EXISTS `qrs`;
CREATE TABLE `qrs` (
  `id` int(10) UNSIGNED NOT NULL,
  `class_date` date NOT NULL,
  `lecturer_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `module_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `intake_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `class_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `intake_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_type` int(11) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `name`, `intake_id`, `user_type`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'kavier@test.com', '$2y$10$Lhv2z0yc9/ryBXNTOhA40Okvm.DrQApwF9wzwKwEJGR2VgZf7tyUC', 'Kavier', '', 1, 'S1lgVRisbVb3YrHUxGByrDNkmyKtZ2o9ZOCtaAfOuICx6A16zGIiZBLrUfNx', '2016-10-22 04:22:27', '2017-02-20 02:15:53'),
(2, 'admin@qrats.com', '$2y$10$.FkCYUrqafHeMOhg4hfSWORiQMQnF0YwoJuJKzcoAZEjGv6ChBdHm', 'Admin1', '', 1, NULL, '2017-02-20 02:05:57', '2017-02-20 02:05:57'),
(3, 'lecturer1@qrats.com', '$2y$10$KgeBofzJsHdi1eSB1.eIhObct4cjfQA0fORvfPMM0pv5q.SniH4r.', 'Lecturer 1', '', 2, NULL, '2017-02-20 02:06:11', '2017-02-20 02:06:11'),
(4, 'lecturer2@qrats.com', '$2y$10$4LnmDjAVe7SjtVQpW8nQNOfjaEPUHygsUdjbhBjbxuXZ6oYi2Mvf6', 'Lecturer 2', '', 2, NULL, '2017-02-20 02:06:22', '2017-02-20 02:06:22'),
(5, 'student1@qrats.com', '$2y$10$dnDOf.fzULhHvmVYUFAIv.Ojt1nCoOo3FGH.nBka5FZlE2vkJHWGi', 'Student 1', 'intake_code_1', 3, NULL, '2017-02-20 02:06:40', '2017-02-20 02:14:20'),
(6, 'student2@qrats.com', '$2y$10$4NMpPZEGr6oDE4JuBUyz5OWSPJgsmxzZvQ.Uk8gUAMCUeCclYVFb6', 'Student 2', 'intake_code_2', 3, NULL, '2017-02-20 02:06:56', '2017-02-20 02:14:24'),
(7, 'student3@qrats.com', '$2y$10$2yWLXQMdHWFMcLjGIDPkhuREKEHUjUJGgQldryGrRvHX4LPvUprNK', 'Student 3', 'intake_code_3', 3, NULL, '2017-02-20 02:07:08', '2017-02-20 02:14:28'),
(8, 'student4@qrats.com', '$2y$10$2oQCnBJX.aNYPLa.NZ8vaO0jzKqIDW.Z2k69zxqiwSOc2PXBtvSOe', 'Student 3', 'intake_code_1', 3, NULL, '2017-02-20 02:07:21', '2017-02-20 02:14:31'),
(9, 'student5@qrats.com', '$2y$10$626NY4ZowIr1V4Ie.k/xS.V7ZR6ldEh8DnGRv3FACc.tYNbdDBapC', 'Student 5', 'intake_code_2', 3, NULL, '2017-02-20 02:07:33', '2017-02-20 02:14:35'),
(10, 'student6@qrats.com', '$2y$10$WNKVJUNTt4BYZBZl1J83bOjPH9TtXKzUbEtdsr2TOs/j9gNml3bZ6', 'Student 6', 'intake_code_3', 3, NULL, '2017-02-20 02:14:13', '2017-02-20 02:14:39');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `classes`
--
ALTER TABLE `classes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `intakes`
--
ALTER TABLE `intakes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `intakes_intake_code_unique` (`intake_code`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `modules`
--
ALTER TABLE `modules`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `modules_module_code_unique` (`module_code`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `qrs`
--
ALTER TABLE `qrs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `classes`
--
ALTER TABLE `classes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `intakes`
--
ALTER TABLE `intakes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `modules`
--
ALTER TABLE `modules`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `qrs`
--
ALTER TABLE `qrs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
